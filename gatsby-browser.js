/* React stuff */
import React from "react";

/* Components */
import Layout from './src/components/layout';

/* Providers */
import { QueryParamProvider } from 'use-query-params';
import { ContextProvider } from './src/components/context'

export const wrapRootElement = ({ element }) => {
    return (
        <ContextProvider>
            <QueryParamProvider >
                <Layout>
                    {element}
                </Layout>
            </QueryParamProvider>
        </ContextProvider>
    )
};

export const registerServiceWorker = () => true;

export const onClientEntry = () => {
    registerServiceWorker();
};

export const onServiceWorkerUpdateFound = () => {
    registerServiceWorker();
};
