/* React and Gatsby stuff */
import React, { useState } from 'react';

/* Modules */
import styled from 'styled-components';

/* Utils */
import colors from '../utils/colors';

/* Assetes */
import Search from '../assets/svgs/search_white.svg';

/* Interfaces */
import { IFilter } from '../utils/objects';

const SearcherContainer = styled('form')`
    display: flex;
    width: fit-content;
    align-items: center;
    box-shadow: 0 1px 3px rgba(0,0,0,0.06), 0 1px 2px rgba(0,0,0,0.12);
    border-radius: 4px;
    height: 1.8rem;

    & .search-input {
        height: 1.8rem;
        font-size: 0.8rem;
        padding: 0.4rem 0.6rem;
        height: 100%;
        border-radius: 4px 0 0 4px;
        border: none;
        border-top: 1px solid ${colors.blue};
        border-left: 1px solid ${colors.blue};
        border-bottom: 1px solid ${colors.blue};
        outline: none;
    }

    & .search-button {
        padding: 0.1rem 0.9rem;
        border-radius: 0 4px 4px 0;
        border: 1px solid ${colors.blue};
        background: linear-gradient(0deg, rgba(6,10,43,1) 0%, rgba(45,49,80,1) 100%);
        outline: none;
        height: 100%;

        &:hover {
            cursor: pointer;
        }        

        & svg {
            width: 0.54rem;
            height: 0.54rem;
        }
    }

`;


export default function Searcher({ filterBy, setFilterBy }: {
    filterBy: IFilter,
    setFilterBy: React.Dispatch<React.SetStateAction<IFilter>>,
}) {

    const [state, setState] = useState({
        text_to_search: ''
    });

    const searchText = event => {

        event.preventDefault();
        setFilterBy({ ...filterBy, text_to_search: event.target.value })

    };

    return (
        <SearcherContainer onSubmit={event => searchText(event)} className='searcher-container' >
            <input
                className='search-input'
                type='text'
                placeholder='Search'
                onChange={event => setState({ ...state, text_to_search: event.target.value })}
            />

            <button className='search-button'>
                <Search />
            </button>
        </SearcherContainer>
    )
}
