/* Ract & Gatsby stuff */
import React from 'react';
import { Link } from 'gatsby';

/* Modules */
import styled from 'styled-components';

/* Utils */
import colors from "../utils/colors";

/* Assets */
import Close from '../assets/svgs/cancel_white.svg';
import Mechanics from '../assets/svgs/mechanics.svg';
import Gears from '../assets/svgs/gears.svg';
import Trade from '../assets/svgs/trade.svg';
import Dashboard from '../assets/svgs/dashboard.svg';
import Helmet from '../assets/svgs/helmet.svg';
import Vehicle from '../assets/svgs/motorbike.svg';
import Logout from '../assets/svgs/logout.svg';

const SideMenuContainer = styled('section')`
  height: fit-content;
  width: 14.2rem;
  background-color: ${colors.gray_base};
  position: fixed;
  z-index: 1;
  top: 0;
  left: 100vw;
  display: flex;
  align-items: center;
  flex-direction: column;
  border-radius: 0 0 0 4px;
  transition: all 0.3s cubic-bezier(.25,.8,.25,1);
  
  & .title-container {
    width: 100%;
    padding: 0.6rem;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    background-color: ${colors.blue};
    border-bottom: 1px solid ${colors.blue};
    
    & button {
      padding: calc(0.6rem + 1px);
      border: none;
      background-color: transparent;
      display: flex;
      align-items: center;
    }

    & button:hover {
      cursor: pointer;
    }
    
    & svg {
      max-width: 0.8rem;
      max-height: 0.8rem;
    }
  }

  & .side-menu-list {
    margin: 0;
    width: 100%;
    height: fit-content;
    background-color: ${colors.gray_base};
    display: flex;
    flex-direction: column;
    align-items: center;
    border: 1px solid ${colors.blue};
    border-top: 0;
    border-right: 0;
    border-radius: 0 0 0 4px;
    box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
    overflow: scroll;

    & div {
      width: 100%;
    }

    & .side-menu-item {
      background-color: ${colors.white};
      display: flex;
    }

    & a {
      width: 100%;
      color: ${colors.black};
      text-decoration-line: none;
    }

    & .powered-by {
      padding: 2rem;
      font-size: 0.6rem;
    }

    & li {
      width: 100%;
      list-style: none;
      padding: 1rem;
      display: flex;
      align-items: center;
      justify-content: flex-start;
      margin: 0;
      font-size: 0.9rem;
      border-bottom: 1px solid ${colors.gray_dark};
  
      & svg {
        width: 1rem;
        height: 1rem;
        margin-right: 1rem;
      }
  
    }

    & li:hover {
      cursor: pointer;
      font-weight: bold;
      background-color: ${colors.gray_dark};
    }

    & .side-menu-list-button {
      border: none;
      background-color: transparent;
      width: 100%;
      padding: 0;
      outline: none;
    }
  }  
`;

export default function SideMenu({ closeMenu }: { closeMenu: Function }) {
    return (
        <SideMenuContainer id='side-menu-container' >

            <div className='title-container' >
                <button onClick={() => closeMenu()} >
                    <Close />
                </button>
            </div>

            <ul className='side-menu-list' >
                <div >
                    {[
                        <Link onClick={() => closeMenu()} to='/dashboard' > <li> <Dashboard /> Dashboard</li> </Link>,
                        <Link onClick={() => closeMenu()} to='/' > <li> <Trade /> Routes Manager</li> </Link>,
                        <Link onClick={() => closeMenu()} to='/drivers' > <li> <Helmet /> Drivers</li> </Link>,
                        <Link onClick={() => closeMenu()} to='/vehicles' > <li> <Vehicle /> Vehicles</li> </Link>,
                        <Link onClick={() => closeMenu()} to='/mechanics' > <li> <Mechanics /> Mechanics</li> </Link>,
                        <Link onClick={() => closeMenu()} to='/settings' > <li> <Gears /> Settings</li> </Link>,
                        <button onClick={() => closeMenu()} className='side-menu-list-button' > <li> <Logout /> Log Out</li> </button>
                    ].map((item, index) => (index % 2 !== 0)
                        ? item
                        : <span className='side-menu-item' >{item}</span>
                    )}
                </div>

                <section className='powered-by'>
                    Powered By ❤️
                </section>

            </ul>
        </SideMenuContainer>
    )
}
