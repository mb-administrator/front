/* React and Gatsby stuff */
import React from 'react';

/* Components */
import Context from './context';
import Button from './button';

/* Modules */
import styled from 'styled-components';

/* Utils */
import colors from '../utils/colors';

const ComingSoonContainer = styled('section')`
    width: 100%;
    height: calc(100vh - 3.5rem);
    min-height: 580px;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: ${colors.gray_base};
    color: ${colors.white};
    padding-top: 3.5rem;
    position: relative;
    
    & .card {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        width: fit-content;
        height: fit-content;
        padding: 2.4rem;
        border: 1px solid ${colors.black};
        background-color: ${colors.blue};
        border-radius: 4px;
        box-shadow: 0 8px 8px rgba(0,0,0,0.16), 0 4px 4px rgba(0,0,0,0.14);
        font-size: 0.9rem;

        & svg {
            width: 1.2rem;
            height: 1.2rem;
            margin-right: 0.8rem;
        }

        & .card-title {
            display: flex;
            align-items: center;
            justify-content: center;
            margin-bottom: 0.8rem;
        }

        & .card-subtitle {
            font-size: 0.9rem;
        }

        & h2 {
            font-size: 0.9rem; 
            margin: 0;
        }

        & span {
            margin: 0.8rem;
            font-size: 1.2rem;
        }

        & form {
            margin-top: 1rem;
            width: 100%;
            display: flex;
            align-items: center;    
            justify-content: center;
            flex-direction: column;
            padding-bottom: 0.6rem;

            & label {
                width: 100%;
                display: flex;
                align-items: center;    
                justify-content: center;
                flex-direction: column;
                display: block;
            }

            & input {
                width: 100%;
                padding: 0.4rem 0.8rem;
                border-radius: 4px;
                border: 1px solid ${colors.gray_dark};
                outline: none;
            }

        }

        & .notify-message {
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;
            background-color: ${colors.green};
            padding: 0.4rem;
            border-radius: 4px;
            margin-top: 2rem;
        }

    }

    & .created-by {
        position: absolute;
        bottom: 2rem;
        color: ${colors.black};
        font-size: 0.8rem;
    }

`;

export default function ComingSoon({ Icon, title }: { Icon: any, title: string }) {

    const handleNewUserNotify = ({ setState, state }, event) => {
        event.preventDefault();
        setState({ ...state, notify_user: true });
    };

    return (
        <ComingSoonContainer>

            <section className='card' >

                <div className='card-title' ><Icon /> <h2>{title}</h2></div>
                <p className='card-subtitle' ><span>🚧 </span> Under Construction <span>🚧 </span></p>

                <Context.Consumer >
                    {({ state, setState, notify_user }) => {

                        if (!notify_user) {

                            return (
                                <form onSubmit={event => handleNewUserNotify({ setState, state }, event)} >
                                    <label>
                                        <p>Email:</p>
                                        <input type='email' placeholder='Email' required />
                                    </label>

                                    <Button red text="Notify Me When It's Ready" />
                                </form>
                            );

                        } else {

                            return <div className='notify-message' >OK, We'll Notify You</div>

                        };

                    }}
                </Context.Consumer>

            </section>

            <p className='created-by' ><b>Created By:</b> mcontreras.se95@gmail.com</p>

        </ComingSoonContainer>
    )
};
