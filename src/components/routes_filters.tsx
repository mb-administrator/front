/* React & Gatsby stuff */
import React from 'react';

/* Modules */
import styled from 'styled-components';

/* Componets */
import Context from '../components/context';
import Searcher from './searcher';
import Ordener from './ordener';

/* Interfaces */
import { IFilter } from './routes';

const FilterConatiner = styled('div')`
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;    
    max-width: 1440px;
    flex-wrap: wrap;
    flex-direction: row-reverse;
    padding: 1rem 0.6rem 0 0.6rem;

    & .routes-remaining {
        font-size: 0.9rem;
    }
`;

export default function Filter({ filterBy, setFilterBy }: {
    filterBy: IFilter,
    setFilterBy: React.Dispatch<React.SetStateAction<IFilter>>,
}) {

    return (
        <FilterConatiner >

            <Searcher filterBy={filterBy} setFilterBy={setFilterBy} />

            <p className='routes-remaining' >
                <Context.Consumer>
                    {({ available_routes }) => (
                        <b>{available_routes} / 25</b>
                    )}
                </Context.Consumer>

                <span>Available Routes</span>
            </p>

            <Ordener filterBy={filterBy} setFilterBy={setFilterBy} />

        </FilterConatiner>
    );

};
