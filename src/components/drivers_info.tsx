/* React & Gatsby stuff */
import React from 'react';

/* Modules */
import styled from 'styled-components';
import Clock from 'react-digital-clock';

/* Components */
import Context from '../components/context';
import Tooltip from '../components/tooltip';

/* utils */
import colors from "../utils/colors";
import { showAndHiddeMenu } from '../utils/functions';

/* Assets */
import HelmetWhite from '../assets/svgs/helmet_white.svg';

const DriversInfoContainer = styled('section')`
  width: 100vw;
  background-color: ${colors.blue};
  display: flex;
  align-items: flex-start;
  justify-content: center;
  padding: 1rem;
  top: 3.4rem;
  position: fixed;
  transition: top 0.3s;
  box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  z-index: 1;
  color: ${colors.white}; 

  & .title-container {
    position: absolute;
    left: 1rem;

    & p {
      font-size: 0.9rem;
    }
  }
    
  & .driver-counter {
    display: flex;
    align-items: center;

    & svg {
      width: 1rem;
      height: 1rem;
    }

    & .value {
      display: flex;
      justify-content: center;
      font-size: 1rem;
      font-weight: bold;
      margin: 0 0.4rem;
    }
  }

  & .clock-container {
    display: flex;
    align-items: center;
    position: absolute;
    right: 0.8rem;
    font-size: 0.9rem;

    &:hover {
      cursor: pointer;
    }
  }
`;

export default function DriversInfo() {
  return (
    <DriversInfoContainer id='info-header' >

      <div className='title-container'>
        <p>Routes Manager:</p>
      </div>

      <div className='driver-counter'>
        <HelmetWhite />

        <Context.Consumer>
          {({ available_drivers }) => <p className='value' > {available_drivers}</p>}
        </Context.Consumer>
      </div>

      <div className='clock-container' onClick={() => showAndHiddeMenu('tooltip-container')} >

        <Tooltip
          title='What is this time?'
          text={`Drivers will be restarted to 8 every 30min`}
        />

        <Clock />

      </div>

    </DriversInfoContainer>
  )
};
