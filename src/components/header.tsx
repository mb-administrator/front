/* React & Gatsby stuff */
import React, { useEffect } from "react";
import { Link } from 'gatsby';

/* Modules */
import styled, { css } from 'styled-components';
import { window } from 'browser-monads';

/* Utils */
import colors from '../utils/colors';

/* Assets */
import MotorBike from '../assets/svgs/motorbike.svg';
import Menu from '../assets/svgs/menu.svg';

interface IHeaderContainer {
  blue: boolean,
  gray: boolean,
}

const HeaderContainer = styled('header') <IHeaderContainer>`
  width: 100%;
  display: flex;
  justify-content: space-between;
  background-color: ${colors.gray_base};
  color: ${colors.black};
  max-height: 3.4rem; 
  padding: 1rem;
  position: fixed;
  top: 0;
  z-index: 1;
  transition: top 0.3s;

  ${({ blue }) => blue && css`
    background-color: ${colors.blue};
    color: ${colors.white};
  `};

  ${({ gray }) => gray && css`
    background-color: ${colors.gray_base};
    color: ${colors.black};
  `};

  & button {
    font-size: 0.7rem;
    display: flex;
    align-items: center;
    border-radius: 2px;
    border: none;
    transition: all 0.3s cubic-bezier(.25,.8,.25,1);
    line-height: 0;
    background-color: ${colors.gray_base};
    padding: 0;
  }

  & button:hover {
    cursor: pointer;
  }

  & svg {
    height: 1.4rem;
    width: 1.4rem;
  }

  & .brand__container {
    width: fit-content;
    display: flex;
    align-items: center;
    color: ${colors.black};
    text-decoration-line: none;

    & h1 {
      margin: 0 1rem;
      font-size: 0.8rem;
      font-weight: bold;
    }
  }
`;

/* Header Animation */
const Header = ({ blue, gray, openHeaderMenu }: { blue?: boolean, gray?: boolean, openHeaderMenu: Function }) => {

  useEffect(() => {

    /* Scroll animation */
    let prevScrollpos = window.pageYOffset;
    window.onscroll = () => {
      try {

        let currentScrollPos = window.pageYOffset;

        if (prevScrollpos > currentScrollPos) {

          document.getElementById("navbar").style.top = "0";
          document.getElementById("info-header").style.top = "3.4rem";

        } else {

          document.getElementById("navbar").style.top = "-3.4rem";
          document.getElementById("info-header").style.top = "0";

        };

        prevScrollpos = currentScrollPos;

      } catch {  };

    };
  }, []);

  return (
    <HeaderContainer
      id='navbar'
      blue={blue ? blue : null}
      gray={gray ? gray : null}
    >
      <Link to='/' className='brand__container' >
        <MotorBike />
        <h1>MB Administrator</h1>
      </Link>

      <button onClick={() => openHeaderMenu()} >
        <Menu />
      </button>

    </HeaderContainer>
  );
};

export default Header;
