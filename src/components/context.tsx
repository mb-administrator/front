/* React and Gatsby stuff */
import React, { useState, useEffect, createContext } from "react"

const Context = createContext({
    available_drivers: undefined,
    available_routes: undefined,
    notify_user: undefined,
    state: undefined,
    setState: undefined,
});

const ContextProvider = ({ children }) => {

    const [state, setState] = useState({
        available_drivers: null,
        available_routes: null,
        notify_user: false,
    });

    useEffect(() => {
        setState({ ...state, available_drivers: 7, available_routes: 24 });
    }, []);

    return (
        <Context.Provider
            value={{
                available_routes: state.available_routes,
                available_drivers: state.available_drivers,
                notify_user: state.notify_user,
                state: state,
                setState: setState,
            }}
        >
            {children}
        </Context.Provider>
    )

};

export default Context;
export { ContextProvider };
