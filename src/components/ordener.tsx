/* React & Gatsby stuff */
import React, { useEffect } from 'react';

/* Modules */
import styled from 'styled-components';
import { useQueryParam, StringParam } from 'use-query-params';

/* Utils */
import colors from '../utils/colors';
import { showAndHiddeMenu } from '../utils/functions';

/* Assets */
import Caret from '../assets/svgs/caret_black.svg';

/* Interfaces */
import { IFilter } from '../utils/objects';

const OrdenerConatiner = styled('div')`
    width: 10rem;
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    &  .ordener-menu {
        font-size: 0.9rem;
        padding: 0.8rem 0.8rem 0.4rem 0;
        display: flex;
        align-items: center;
        width: 10rem;

        & div {
            width: 10rem;
        }
        
        & svg {
            width: 0.6rem;
            height: 0.6rem;
            margin: 0 0 0 0.4rem;
        }
        
    
        &:hover {
            cursor: pointer;
        }

    }

    & #ordener-menu-items {
        width: 100%;
        position: absolute;
        top: 2.3rem;
        right: 0;
        background-color: ${colors.white};
        border-radius: 4px;
        box-shadow: 0 1px 3px rgba(0,0,0,0.06), 0 1px 2px rgba(0,0,0,0.12);
        display: none;
        border: 1px solid ${colors.black};

        & .option-selected {
            background-color: ${colors.gray_base};
        }

        & .first {
            border-radius: 4px 4px 0 0;
        }

        & .last {
            border-radius: 0 0 4px 4px;
        }

        & li {
            margin: 0;
            list-style: none;
            padding: 0.6rem 0.6rem;
            display: flex;
            align-items: center;
            justify-content: center;
            font-size: 0.9rem;
        }

        & li:hover {
            cursor: pointer;
            background-color: ${colors.gray_dark};
        }

    }
`;

export default function Ordener({ filterBy, setFilterBy }: {
    filterBy: IFilter,
    setFilterBy: React.Dispatch<React.SetStateAction<IFilter>>,
}) {

    const { order_by } = filterBy;
    const [queryOrderBy, setQueryOrderBy] = useQueryParam('order-by', StringParam);

    const handleOrdenerBy = (by: string) => setFilterBy({ ...filterBy, order_by: by });

    useEffect(() => {
        setQueryOrderBy(order_by.toLowerCase());
        handleOrdenerBy(order_by);
    }, [order_by]);

    /* Fiter by query url */
    useEffect(() => {
        queryOrderBy && handleOrdenerBy(queryOrderBy[0].toLocaleUpperCase() + queryOrderBy.substring(1));
    }, []);

    return (
        <OrdenerConatiner className='ordener-container' onClick={() => showAndHiddeMenu('ordener-menu-items')} >
            
            <div className='ordener-menu' >
                <b>Order By:</b>

                <div >
                    {order_by}
                </div>

                <Caret />
            </div>

            <ul id='ordener-menu-items' >
                <li
                    className={`first ${order_by === 'Date - asc' && 'option-selected'}`}
                    onClick={() => handleOrdenerBy('Date - asc')} >
                    Date - asc
                </li>

                <li
                    className={`${order_by === 'Date - des' && 'option-selected'}`}
                    onClick={() => handleOrdenerBy('Date - des')} >
                    Date - des
                </li>

                <li
                    className={`${order_by === 'Name - a-z' && 'option-selected'}`}
                    onClick={() => handleOrdenerBy('Name - a-z')} >
                    Name - a-z
                </li>

                <li
                    className={`${order_by === 'Name - z-a' && 'option-selected'}`}
                    onClick={() => handleOrdenerBy('Name - z-a')} >
                    Name - z-a
                </li>

                <li
                    className={`${order_by === 'Ranking - asc' && 'option-selected'}`}
                    onClick={() => handleOrdenerBy('Ranking - asc')} >
                    Ranking - asc
                </li>

                <li
                    className={`last ${order_by === 'Ranking - des' && 'option-selected'}`}
                    onClick={() => handleOrdenerBy('Ranking - des')} >
                    Ranking - des
                </li>
            </ul>

        </OrdenerConatiner>
    )
};
