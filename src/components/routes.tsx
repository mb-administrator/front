/* React & Gatsby stuff */
import React, { useEffect, useState } from 'react';

/* Modules */
import styled from 'styled-components';

/* Components */
import RouteCard from './route_card';
import RoutesFilter from './routes_filters';

/* Utils */
import colors from '../utils/colors';

/* Interfaces */
import { IFilter } from '../utils/objects';

const CardsContainer = styled('section')`
    width: 100%;
    height: fit-content; 
    background-color: ${colors.gray_base};
    padding: 0.4rem;
    padding-top: 6.8rem;
    display: flex;
    align-items: center;
    flex-direction: column;
    
    & .routes-cards-container {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        width: 100%;
        max-width: 1440px;
    }

`;

let allRoutes = [];
export default function Routes() {

    /* Loading state */
    const [state, setState] = useState({
        loading: false,
    });

    /* Params to filter state */
    const [filterBy, setFilterBy] = useState<IFilter>({
        text_to_search: '',
        order_by: 'Date - asc',
    });

    useEffect(() => {

        setState({ ...state, loading: true });

        allRoutes = [];

        /* Crafting cards */
        let initial_hour = 8;
        let minutes = 0;
        let type = 'a. m.';

        for (let i = 1; i < 26; i++) {

            allRoutes.push(
                <RouteCard
                    user_name='Mario ECS'
                    rate='4.77'
                    driver='Mario Eduardo Contreras Serrano'
                    phone='56 1371 5678'
                    location='56 1371 5678'
                    destination='56 1371 5678'
                    delivery_date='21/01/21'
                    price='145.00'
                    shares='3'
                    comments='7'
                    initial_hour={initial_hour}
                    minutes={minutes}
                    type={type}
                    active={i === 3 && true}
                    bad={i === 5 && true}
                    disabled
                    purchased_by='SnowMoon 27'
                />
            );

            /* Hour handler to every 30min */
            if (i % 2 === 0) { initial_hour++; minutes = 0; } else minutes = 30;
            if (initial_hour === 13) { initial_hour = 1; type = 'p. m.'; };

        };

        setState({ ...state, loading: false });

    }, [filterBy]);

    return (
        <CardsContainer>

            <RoutesFilter filterBy={filterBy} setFilterBy={setFilterBy} />

            <div className='routes-cards-container' >
                {
                    state.loading
                        ? <p>Loading...</p>
                        : allRoutes.map(route => route)
                }
            </div>

        </CardsContainer>
    )
};

export { IFilter };
