/* React & Gatsby stuff */
import React from 'react';

/* Modules */
import styled, { css } from 'styled-components';

/* Components */
import Context from '../components/context';
import Button from '../components/button';

/* Assets */
import User from '../assets/svgs/user.svg';

/* Utils */
import colors from '../utils/colors';

interface ICard {
    active: boolean,
    bad: boolean,
    disabled: boolean,
};

const Card = styled('section') <ICard>`
    width: 24rem;
    background-color: ${colors.white};
    margin: 0.8rem 0.6rem;
    border-radius: 5px;
    box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
    transition: all 0.3s cubic-bezier(.25,.8,.25,1);
    display: flex;
    align-items: flex-start;
    justify-content: flex-start;
    font-size: 1.2rem;
    flex-direction: column;

    &:hover {
        ${({ disabled }) => !disabled && css`
            box-shadow: 0 8px 8px rgba(0,0,0,0.16), 0 4px 4px rgba(0,0,0,0.14);
        `}
    }

    & .card-hour-and-title {
        border: 1px solid ${colors.black};
        padding: 0.6rem 0.8rem;
        font-size: 0.9rem;
        border-radius: 4px 4px 0 0;
        background-color: ${colors.black};
        width: 100%;
        color: ${colors.white};
        display: flex;
        justify-content: space-between; 
    }

    & .card-title {
        display: flex;
        align-items: center;
        width: fit-content;

        & svg {
            width: 1rem;
            height: 1rem;
            margin-right: 0.6rem;
        }
    }

    & .card-data {
        margin-left: 1rem;
        margin-bottom: 0;
        padding: 0.8rem;

        & li {
            font-size: 0.9rem;
            margin: 0;
        }
    }

    & .card-stats-container {
        width: 100%;
        display: flex;
        justify-content: space-between;
        padding: 0 0.8rem 0 0.8rem;

        & .purchased-by {
            color: ${colors.black};
            font-size: 0.7rem;
        }

        & div {
            display: flex;
        }

        & p {
            font-size: 0.8rem;
            color: ${colors.blue_high};
        }

        & p:hover {
            cursor: pointer;
            text-decoration: underline;
        }

    }

    & .card-actions-container {
        width: 100%;
        display: flex;
        justify-content: space-between;
        padding: 0.8rem;
    }

    ${props => props.active && css`
        border: 1px solid ${colors.green};
        background-color: ${colors.green_light};
    `}

    ${props => props.bad && css`
        border: 1px solid ${colors.red};
        background-color: ${colors.red_light};
    `}

    @media screen and (max-width: 921px) {
        & {
            width: 100%;
        }
    }
`;

export default function RouteCard(
    { initial_hour, minutes, type, active, bad, driver, phone, location, destination, delivery_date, price, user_name, rate, shares, comments, disabled, purchased_by }: {
        initial_hour: number,
        minutes: number,
        type: string,
        active?: boolean,
        bad?: boolean,
        driver: string,
        phone: string,
        location: string,
        destination: string,
        delivery_date: string,
        price: string,
        user_name: string,
        rate: string,
        shares: string,
        comments: string,
        disabled: boolean,
        purchased_by: string,
    }) {
    return (
        <Card disabled={disabled} active={active} bad={bad} >

            <div className='card-hour-and-title'>

                <div className='card-title'>
                    <User />
                    <p>{user_name} - {rate}</p>
                </div>

                <p>{`${initial_hour < 10 ? `0${initial_hour}` : initial_hour}:${minutes === 0 ? `0${minutes}` : minutes} ${type}`}</p>

            </div>

            <ul className='card-data'>
                <li><p><b>Driver:</b> {driver}</p></li>
                <li><p><b>Phone:</b> {phone}</p></li>
                <li><p><b>Location:</b> {location}</p></li>
                <li><p><b>Destination:</b> {destination}</p></li>
                <li><p><b>Delivery Date:</b> {delivery_date}</p></li>
                <li><p><b>Price:</b> ${price}</p></li>
            </ul>

            <div className='card-stats-container'>
                <div>
                    { purchased_by ? <p className='purchased-by' ><b>Purchased By:</b> SnowMoon27.</p> : null}
                </div>
                
                <div>
                  <p>({shares}) Share</p>
                  <p style={{ margin: '0 0.3rem' }} ></p>
                  <p> ({comments}) Comment</p>
                </div>
            </div>

            <div className='card-actions-container'>
                <Context.Consumer>
                    {({ available_routes, available_drivers, state, setState }) => (

                        <Button
                            disabled
                            yellow
                            text='Select Route'
                            handleFunction={() => {
                                setState({
                                    ...state,

                                    available_drivers: available_drivers !== 0
                                        ? available_drivers - 1
                                        : available_drivers,

                                    available_routes: available_drivers !== 0
                                        ? available_routes - 1
                                        : available_routes,

                                });
                            }}
                        />

                    )}
                </Context.Consumer>
            </div>
        </Card>
    )
};
