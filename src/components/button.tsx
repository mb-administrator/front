/* React & Gatsby stuff */
import React from 'react';

/* Modules */
import styled, { css } from 'styled-components';

/* Utils */
import colors from '../utils/colors';

interface IButtonContainer {
    yellow?: boolean,
    red?: boolean,
};

const ButtonContainer = styled('button') <IButtonContainer>`

    width: 100%;
    font-size: 0.9rem;
    border: none;
    padding: 0.4rem ;
    border-radius: 4px;

    &:hover {
        cursor: pointer;
        box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
    }
    
    ${({ yellow }) => yellow && css`
        background: linear-gradient(180deg, rgba(243,221,168,1) 0%, rgba(234,194,98,1) 100%);
        border: 1px solid ${colors.brown};
    `}
    
    ${({ red }) => red && css`
        background: linear-gradient(180deg, rgba(187,57,56,1) 0%, rgba(188,43,41,1) 100%);
        color: ${colors.white};
        margin-top: 1.6rem;
    `}

    ${({ disabled }) => disabled && css`
        background: ${colors.gray_dark};
        color: ${colors.black};

        &:hover {
            cursor: not-allowed;
            box-shadow: none;
        }

    `}
`;

export default function Button({ handleFunction, text, yellow, red, disabled }: {
    handleFunction?: Function,
    text: string,
    yellow?: boolean,
    red?: boolean,
    disabled?: boolean,
}) {
    return (
        <ButtonContainer
            onClick={() => handleFunction ? handleFunction() : null}
            yellow={yellow}
            red={red}
            disabled={disabled}
        >
            {text}
        </ButtonContainer>
    )
};
