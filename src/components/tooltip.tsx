/* React & Gatsby stuff */
import React from 'react';

/* Modules */
import styled from 'styled-components';

/* Assets */
import HelmetWhite from '../assets/svgs/helmet_white.svg';
import Info from '../assets/svgs/info.svg';

/* Utils */
import colors from '../utils/colors';

const TooltipIconMenuContainer = styled('div')`
    display: flex;
    align-items: center;
    position: relative;
    width: fit-content;
    height: 100%;

    & .tooltip-icon {
        margin-right: 0.4rem;
        width: 0.7rem;
        height: 0.7rem;
    }

    & #tooltip-container {
      background-color: ${colors.black_transparent};
      padding: 1rem;
      position: absolute;
      right: -240%;
      border-radius: 4px;
      height: fit-content;
      bottom: -8.8rem;
      width: 12rem;
      box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
      display: flex;
      justify-content: center;
      align-items: center;
      flex-direction: column;
      display: none;

      & .tooltip-title {
        font-size: 1rem;
        margin-bottom: 0.6rem;
      }

      & .triangle {
        display: block;
        position: absolute;
        width: 0.8rem;
        height: 0.6rem;
        background-color: ${colors.black_transparent};
        top: -0.5rem;
        right: 25%;
        clip-path: polygon(50% 0%, 100% 100%, 0% 100%);
        box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
      }

      & .tooltip-helmet-icon {
        margin: 0 0.2rem;
        height: 0.7rem;
        width: 0.7rem;
      }
    }
`;

export default function Tooltip({ title, text }:
    {   
        title: string,
        text: string,
    }) {
    return (
        <TooltipIconMenuContainer >

            <Info className='tooltip-icon' />

            <div id='tooltip-container'>
                <span className='triangle' />
                <p className='tooltip-title'>{title}</p>
                <p className='tooltip-text'>{text}</p>
            </div>

        </TooltipIconMenuContainer>
    )
};
