/* React & Gatsby stuff */
import React from "react";
import PropTypes from "prop-types";

/* Modules */
import { document } from 'browser-monads';

/* Components */
import Header from "./header";
import SideMenu from '../components/side_menu';

/* Styles */
import "./layout.css";

const Layout = ({ children }) => {

  const openHeaderMenu = () => document.getElementById('side-menu-container').style.left = 'calc(100vw - 14.2rem)';
  const closeHeaderMenu = () => document.getElementById('side-menu-container').style.left = 'calc(100vw - 0rem)';

  return (
    <span style={{ position: 'relative', width: '100%' }} >

      <Header gray openHeaderMenu={openHeaderMenu} />

      <div>
        <main>{children}</main>
      </div>

      <SideMenu closeMenu={closeHeaderMenu} />

    </span>
  );
  };

  Layout.propTypes = {
    children: PropTypes.node.isRequired,
  };

  export default Layout;
