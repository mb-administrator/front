/* React and Gatsby stuff */
import React from 'react';

/* Components */
import SEO from "../components/seo";
import ComingSoon from "../components/coming_soon";

/* Assets */
import MechanicsIcon from '../assets/svgs/mechanics_white.svg';

export default function Mechanics() {
    return (
        <>
            <SEO title={'Mechanics'} />
            <ComingSoon title='Mechanics' Icon={MechanicsIcon} />
        </>
    )
};
