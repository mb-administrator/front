/* React and Gatsby stuff */
import React from 'react';

/* Components */
import SEO from "../components/seo";
import ComingSoon from "../components/coming_soon";

/* Assets */
import MotorBike from '../assets/svgs/motorbike_white.svg';

export default function Vehicles() {
    return (
        <>
            <SEO title={'Vehicles'} />
            <ComingSoon title='Vehicles' Icon={MotorBike} />
        </>
    )
}
