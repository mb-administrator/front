/* React and Gatsby stuff */
import React from 'react';

/* Components */
import SEO from "../components/seo";
import ComingSoon from "../components/coming_soon";

/* Assets */

import DriversIcon from '../assets/svgs/helmet_white.svg';

export default function Drivers() {
    return (
        <>
            <SEO title={'Drivers'} />
            <ComingSoon title='Drivers' Icon={DriversIcon} />
        </>
    )
};
