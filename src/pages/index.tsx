/* React & Gatsby stuff */
import React from "react";

/* Components */
import SEO from "../components/seo";
import DriversInfo from '../components/drivers_info';
import Routes from '../components/routes';

/* --- Path '/' For Routes Manager --- */
const IndexPage = () => (
    <>

      <SEO title="Routes Manager" />
      <DriversInfo />
      <Routes />

    </>
  );

export default IndexPage;
