/* React and Gatsby stuff */
import React from 'react';

/* Components */
import SEO from "../components/seo";
import ComingSoon from "../components/coming_soon";

/* Assets */
import Dasboard from '../assets/svgs/dashboard_white.svg';

export default function Dashboard() {
    return (
        <>
            <SEO title={'Dashboard'} />
            <ComingSoon title='Dashboard' Icon={Dasboard} />
        </>
    )
};
