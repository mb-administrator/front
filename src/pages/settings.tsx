/* React and Gatsby stuff */
import React from 'react';

/* Components */
import SEO from "../components/seo";
import ComingSoon from "../components/coming_soon";

/* Assets */
import SettingsIcon from '../assets/svgs/settings_white.svg';

export default function Settings() {
    return (
        <>
            <SEO title={'Settings'} />
            <ComingSoon title='Settings' Icon={SettingsIcon} />
        </>
    )
};
