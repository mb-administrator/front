/* Full page colors */

let colors = {
    gray_base: '#F1F1F1',
    gray_dark: '#e6e0e0',
    black_transparent: 'rgba(13, 14, 14, 0.9)',
    black: 'rgb(13, 14, 14)',
    white: 'rgb(251, 251, 251)',
    green_light: '#c8e6c9',
    green: '#0e661c',
    red: '#be312f',
    red_light: '#ffcdd2',
    blue: 'rgb(6, 10, 43)',
    blue_high: 'rgb(41, 58, 115)',
    brown: 'rgb(129, 106, 52)',
    pink: 'rgb(213, 68, 66)',
};

export default colors;
