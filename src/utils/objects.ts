/* Interfaces */
interface IFilter {
    text_to_search?: string,
    order_by?: string,
};

export { IFilter };